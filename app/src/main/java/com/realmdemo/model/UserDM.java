package com.realmdemo.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by User on 9/21/2016.
 */

public class UserDM  extends RealmObject{
    @PrimaryKey
    private int id;
    private String name="";
    private String country="";
    private long salary;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }
}
