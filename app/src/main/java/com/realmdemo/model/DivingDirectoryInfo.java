package com.realmdemo.model;

public class DivingDirectoryInfo {
	
	private String id="";
	private String name="";
	private String description="";
	private String address="";
	private String image_url="";
	private String[] activity_ids;
	private String area="";
	private String city="";
	private String lat="";
	private String lon="";
	private String email="";
	private String fax="";
	private String phone="";
	private String postal_code="";
	private String primary_contact="";
	private String rates="";
	private String website="";
	private String islandid="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String[] getActivity_ids() {
		return activity_ids;
	}
	public void setActivity_ids(String[] activity_ids) {
		this.activity_ids = activity_ids;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	public String getPrimary_contact() {
		return primary_contact;
	}
	public void setPrimary_contact(String primary_contact) {
		this.primary_contact = primary_contact;
	}
	public String getRates() {
		return rates;
	}
	public void setRates(String rates) {
		this.rates = rates;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getIslandid() {
		return islandid;
	}
	public void setIslandid(String islandid) {
		this.islandid = islandid;
	}
	
	
	

}
