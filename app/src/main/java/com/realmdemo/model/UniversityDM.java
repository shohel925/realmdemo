package com.realmdemo.model;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TonuJewel on 10/23/2016.
 */

public class UniversityDM extends RealmObject {
    @PrimaryKey
    private String  id;
    private String universityName="";
    List<StudentDM> studentDM=new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public List<StudentDM> getStudentDM() {
        return studentDM;
    }

    public void setStudentDM(List<StudentDM> studentDM) {
        this.studentDM = studentDM;
    }
}
