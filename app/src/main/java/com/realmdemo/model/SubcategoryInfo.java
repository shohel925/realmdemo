package com.realmdemo.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class SubcategoryInfo {
	@SerializedName("id")
	String id = "";
	@SerializedName("subcategoryname")
	String subcategoryname = "";
	@SerializedName("parentId")
	String parentId = "";
	@SerializedName("subcategorylist")
	List<SecondSubcategoryInfo> subcategorylist;
	public List<SecondSubcategoryInfo> getSubcategorylist() {
		return subcategorylist;
	}

	public void setSubcategorylist(List<SecondSubcategoryInfo> subcategorylist) {
		this.subcategorylist = subcategorylist;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubcategoryname() {
		return subcategoryname;
	}

	public void setSubcategoryname(String subcategoryname) {
		this.subcategoryname = subcategoryname;
	}

}
