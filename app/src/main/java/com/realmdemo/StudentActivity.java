package com.realmdemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.realmdemo.adapter.DividerItemDecoration;
import com.realmdemo.model.StudentDM;
import com.realmdemo.model.UniversityDM;
import com.realmdemo.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class StudentActivity extends AppCompatActivity {
    private Context context;
    private EditText sName,sRoll,sSubject;
    private RecyclerView recyclerStudent;
    private StudentAdapter userAdapter;
    private List<StudentDM> tempList=new ArrayList<>();
    private Realm realm;
    private UniversityDM universityDM;
    private StudentDM dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student);
        context=this;
        realm=Realm.getDefaultInstance();
        initUI();
    }

    private void initUI() {
        sName=(EditText)findViewById(R.id.sName) ;
        sRoll=(EditText)findViewById(R.id.sRoll);
        sSubject=(EditText)findViewById(R.id.sSubject);
        recyclerStudent=(RecyclerView)findViewById(R.id.recyclerStudent);
        if (tempList!=null){
            tempList.clear();
        }
        // get all data from RealM ===============
        universityDM= realm.where(UniversityDM.class).equalTo("id", AppConstant.universityId).findFirst();
        tempList=universityDM.getStudentDM();
        userAdapter=new StudentAdapter(tempList);
        recyclerStudent.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerStudent.setLayoutManager(mLayoutManager);
        recyclerStudent.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerStudent.setItemAnimator(new DefaultItemAnimator());
        recyclerStudent.setAdapter(userAdapter);
    }

    public void addButton(View view) {
        if (TextUtils.isEmpty(sName.getText().toString())){
            Toast.makeText(context,"Please enter Student Name",Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(sRoll.getText().toString())) {
            Toast.makeText(context,"Please enter Roll ",Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(sSubject.getText().toString())) {
            Toast.makeText(context,"Please enter Subject ",Toast.LENGTH_SHORT).show();
        }else
        {
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    dm=new StudentDM();
                    dm.setName(sName.getText().toString());
                    dm.setRoll(sRoll.getText().toString());
                    dm.setSubject(sSubject.getText().toString());
                    universityDM.getStudentDM().add(dm);
                    realm.copyToRealmOrUpdate(universityDM);

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    tempList.add(dm);
                    userAdapter.notifyDataSetChanged();
                    // Original queries and Realm objects are automatically updated.
                }
            });
        }
    }


    public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {

        private List<StudentDM> userList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView sName,roll,subject;

            public MyViewHolder(View view) {
                super(view);
                sName = (TextView) view.findViewById(R.id.sName);
                roll = (TextView) view.findViewById(R.id.roll);
                subject = (TextView) view.findViewById(R.id.subject);
            }
        }


        public StudentAdapter(List<StudentDM> moviesList) {
            this.userList = moviesList;
        }

        @Override
        public StudentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.student_row, parent, false);

            return new StudentAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(StudentAdapter.MyViewHolder holder, final int position) {
            final StudentDM userDM = userList.get(position);
            holder.sName.setText(userDM.getName());
            holder.roll.setText(userDM.getRoll());
            holder.subject.setText(userDM.getSubject());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("Log ","Student Item CLick");
                }
            });
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }
    }

}
