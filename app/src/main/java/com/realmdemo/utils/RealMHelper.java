package com.realmdemo.utils;

import com.realmdemo.model.UserDM;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.internal.Context;

/**
 * Created by User on 9/26/2016.
 */

public class RealMHelper {
    public Realm realm;

    public RealMHelper(){
        realm=Realm.getDefaultInstance();
    }
    //find all objects in the Book.class
    public RealmResults<UserDM> getAllUser(Realm realm) {

        return realm.where(UserDM.class).findAll();
    }
    public void addUser(Realm realm,UserDM userDM) {
        realm.copyToRealm(userDM);
    }
    public void deleteUser(Realm realm,UserDM userDM) {
        realm.copyToRealm(userDM);
    }

    public void editUser(Realm realm,UserDM userDM) {
        realm.copyToRealmOrUpdate(userDM);
    }

}
