package com.realmdemo.utils;

import java.util.Vector;

import com.aapbd.utils.network.KeyValue;
import com.aapbd.utils.network.UrlUtils;

import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class AllURL {

	private static String getcommonURLWithParamAndAction(String action,
			Vector<KeyValue> params) {

		if (params == null || params.size() == 0) {
			return BaseURL.HTTP + action;
		} else {
			String pString = "";

			for (final KeyValue obj : params) {

				pString += obj.getKey().trim() + "=" + obj.getValue().trim()
						+ "&";
			}

			if (pString.endsWith("&")) {
				pString = pString.subSequence(0, pString.length() - 1)
						.toString();
			}

			return BaseURL.HTTP + action + "?" + UrlUtils.encode(pString);
		}
	}

//	public static String getRegisterUrl(String email, String password,
//			String firstName, String lastName) {
//
//		final Vector<KeyValue> temp = new Vector<KeyValue>();
//		temp.addElement(new KeyValue("req=register&email", email));
//		temp.addElement(new KeyValue("pin", password));
//		temp.addElement(new KeyValue("fname", firstName));
//		temp.addElement(new KeyValue("lname", lastName));
//		return getcommonURLWithParamAndAction("", temp);
//
//	}
	public static String getAllIslandUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("islands_list.json", temp);

	}
	
	public static String getGeneralActivityUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("general_activity_list.json", temp);

	}
	public static String getHotelListUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("hotels_list.json", temp);

	}
	public static String getEventListUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("events_list.json", temp);

	}
	
	public static String getMoreInfoListUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("bahamas_more_info.json", temp);

	}
	
	public static String getWeatherUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("weather_info.json", temp);

	}
	
	public static String getActivityDirectoryUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("activities_directory.json", temp);

	}
	
	public static String getFilterApiUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("filtering_options.json", temp);

	}
	public static String getTransportApiUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("transport_info.json", temp);

	}
	public static String getMissingInfo() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("missing_info.json", temp);

	}
	public static String getIslandProperty() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("activity_featured_properties.json", temp);

	}
	


}