package com.realmdemo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.realmdemo.adapter.DividerItemDecoration;
import com.realmdemo.model.UserDM;
import com.realmdemo.utils.RealMHelper;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private EditText name,country,salary;
    private RecyclerView userList;
    private UserAdapter userAdapter;
    private List<UserDM> tempList=new ArrayList<>();
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;
        realm=Realm.getDefaultInstance();
        initUI();
    }

    private void initUI() {
        name=(EditText)findViewById(R.id.name) ;
        country=(EditText)findViewById(R.id.country);
        salary=(EditText)findViewById(R.id.salary);
        userList=(RecyclerView)findViewById(R.id.recycler_view);
        if (tempList!=null){
            tempList.clear();
        }
        // get all data from RealM ===============
        RealmResults<UserDM> data= realm.where(UserDM.class).findAll();
        tempList=realm.copyFromRealm(data);
        userAdapter=new UserAdapter(tempList);
        userList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        userList.setLayoutManager(mLayoutManager);
        userList.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        userList.setItemAnimator(new DefaultItemAnimator());
        userList.setAdapter(userAdapter);
    }

    public void addButton(View view) {
        if (TextUtils.isEmpty(name.getText().toString())){
            Toast.makeText(context,"Please enter name",Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(country.getText().toString())){
            Toast.makeText(context,"Please enter country",Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(country.getText().toString())){
            Toast.makeText(context,"Please enter salary",Toast.LENGTH_SHORT).show();
        }else{
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    UserDM dm=new UserDM();
//                    dm.setId(realm.where(UserDM.class).findAll().size());
                    dm.setName(name.getText().toString());
                    dm.setCountry(country.getText().toString());
                    dm.setSalary(Long.parseLong(salary.getText().toString()));
                    realm.copyToRealmOrUpdate(dm);
                    tempList.add(dm);

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    userAdapter.notifyDataSetChanged();
                    // Original queries and Realm objects are automatically updated.
                }
            });
        }
    }

    public void relationBtn(View view) {
        startActivity(new Intent(context,RelationActivity.class));
    }
    public void universityBtn(View view){
        startActivity(new Intent(context,UniversityActivity.class));
    }


    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

        private List<UserDM> userList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name, counry, salary;
            public Button edit_Btn,deleteBtn;

            public MyViewHolder(View view) {
                super(view);
                name = (TextView) view.findViewById(R.id.name);
                counry = (TextView) view.findViewById(R.id.counry);
                salary = (TextView) view.findViewById(R.id.salary);
                edit_Btn=(Button)view.findViewById(R.id.edit_Btn);
                deleteBtn=(Button)view.findViewById(R.id.deleteBtn);
            }
        }


        public UserAdapter(List<UserDM> moviesList) {
            this.userList = moviesList;
        }

        @Override
        public UserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.user_row, parent, false);

            return new UserAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(UserAdapter.MyViewHolder holder, final int position) {
            final UserDM userDM = userList.get(position);
            holder.name.setText(userDM.getName());
            holder.counry.setText(userDM.getCountry());
            holder.salary.setText(""+userDM.getSalary());
            holder.edit_Btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            userDM.setName("Mamun");
                            realm.copyToRealmOrUpdate(userDM);
                            userList.get(position).setName("Mamun");
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            notifyDataSetChanged();

                        }
                    });

                }
            });
            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            UserDM result = realm.where(UserDM.class).equalTo("id", userDM.getId()).findFirst();
                            result.deleteFromRealm();
                            userList.remove(position);
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            notifyDataSetChanged();
                        }
                    });

                }
            });
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }
    }

}
