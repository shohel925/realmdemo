package com.realmdemo.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.realmdemo.model.CategoryDirectoryInfo;
import com.realmdemo.model.DivingDirectoryInfo;
import com.realmdemo.model.SubcategoryInfo;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "BahamasDB";

	// allChannel table name
	private static final String TABLE_Island = "Island";
	private static final String TABLE_Island_Image = "Island_Image";
	private static final String TABLE_Island_Details = "Island_Details";
	private static final String TABLE_General_Activity = "General_Activity";
	private static final String TABLE_General_Activity_Feature_island = "General_Activity_Feature_island";
	private static final String TABLE_General_Activity_Things_to_Know = "General_Activity_Things_to_Know";
	private static final String TABLE_General_Activity_types_of_activity = "General_Activity_types_of_activity";
	private static final String TABLE_General_Activity_Hotel = "General_Activity_Hotel";
	private static final String TABLE_General_Event = "general_Event";
	private static final String TABLE_General_More = "general_More";
	private static final String TABLE_Category = "Category";
	private static final String TABLE_Category_directory = "Category_directory";
	// DIVING DIRECTORY Table Name
	private static final String TABLE_DIVING_DIR = "DivingDirectoryDB";
	private static final String TABLE_featureProperty = "featureProperty";
	// Transport Table Name and key
	
	private static final String TABLE_TRANSPORT = "transport";
	// ========= for Public Channel ==========================

	private static final String KEY_id = "id";
	private static final String KEY_name = "name";
	private static final String KEY_image_url = "image_url";
	private static final String KEY_logo = "logo";
	private static final String KEY_description = "description";
	private static final String KEY_discover_more = "discover_more";
	private static final String KEY_discover_more_main_image = "discover_more_main_image";
	private static final String KEY_serialid = " serialid";
	private static final String KEY_type = " type";
	private static final String KEY_islandDetaisId = " islandDetaisId";
	private static final String KEY_title = " title";
	private static final String KEY_Activit_realId = " Activit_realId";
	private static final String KEY_image_title = " image_title";
	private static final String KEY_island_id = "island_id";
	private static final String KEY_island_name = "island_name";
	private static final String KEY_island_activity_id = "island_activity_id";
	private static final String KEY_island_activity_title = "island_activity_title";
	private static final String KEY_island_activity_image = "island_activity_image";
	private static final String KEY_island_activity_description = "island_activity_description";
	private static final String KEY_thinks_to_know_Id = "thinks_to_know_Id";
	private static final String KEY_types_of_activity_Id = "types_of_activity_Id";
	private static final String KEY_address = "address";
	private static final String KEY_phone = "phone";
	private static final String KEY_lat = "lat";
	private static final String KEY_lon = "lon";
	private static final String KEY_isFeature = "isFeature";
	private static final String KEY_startDate = "startDate";
	private static final String KEY_endDate = "endDate";
	private static final String KEY_yearly = "yearly";
	private static final String KEY_islandNo = "KEY_islandNo";


	// For Diving Directory Table
	private static final String KEY_dd_id = "dd_id";
	private static final String KEY_dd_name = "dd_name";
	private static final String KEY_dd_description = "dd_description";
	private static final String KEY_dd_address = "dd_address";
	private static final String KEY_dd_image_url = "dd_image_url";
	// private static final String KEY_dd_activity_ids= "dd_activity_ids";
	private static final String KEY_dd_area = "dd_area";
	private static final String KEY_dd_city = "dd_city";
	private static final String KEY_dd_lat = "dd_lat";
	private static final String KEY_dd_lon = "dd_lon";
	private static final String KEY_dd_email = "dd_email";
	private static final String KEY_dd_fax = "dd_fax";
	private static final String KEY_dd_phone = "dd_phone";
	private static final String KEY_dd_postal_code = "dd_postal_code";
	private static final String KEY_dd_primary_contact = "dd_primary_contact";
	private static final String KEY_dd_rates = "dd_rates";
	private static final String KEY_dd_website = "dd_website";
	private static final String KEY_dd_islandid = "islandid";
	private static final String KEY_parentId = "parentId";


	
	private static final String KEY_trans_keyId = "transport_id";
	private static final String KEY_trans_spilt_type = "spilt_type";
	private static final String KEY_trans_island_id = "island_id";
	private static final String KEY_trans_transport_type = "transport_type";
	private static final String KEY_trans_transport_info = "transport_info";
	private static final String KEY_categoryId = "categoryId";
	private static final String KEY_directoryId = "directoryId";
	private static final String KEY_activityId = "activityId";
	private static final String KEY_islandId = "islandId";
	private SQLiteDatabase db = null;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	final String CREATE_featureProperty_TABLE = "CREATE TABLE "
			+ TABLE_featureProperty + "(" + KEY_activityId + " INTEGER  , " + KEY_islandId + " INTEGER  , " + KEY_id
			+ " INTEGER  , "+ KEY_title + " TEXT , " + KEY_address
			+ " TEXT , " + KEY_phone + " TEXT , " + KEY_image_url
			+ " TEXT " + ")";
	
	final String CREATE_Category_TABLE = "CREATE TABLE "
			+ TABLE_Category + "(" + KEY_id + " INTEGER , "
			+ KEY_parentId + " INTEGER , " + KEY_title + " TEXT " + ")";
	
	final String CREATE_Category__Directory_TABLE = "CREATE TABLE "
			+ TABLE_Category_directory + "(" + KEY_directoryId + " INTEGER , "
			+ KEY_categoryId + " INTEGER " + ")";
	
	final String CREATE_General_EVENT_TABLE = "CREATE TABLE "
			+ TABLE_General_Event + "(" + KEY_id + " INTEGER PRIMARY KEY , "
			+ KEY_name + " TEXT , " + KEY_description + " TEXT , "
			+ KEY_island_id + " TEXT , " + KEY_isFeature + " TEXT , "
			+ KEY_startDate + " INTEGER , " + KEY_endDate + " INTEGER , "
			+ KEY_yearly + " TEXT , " + KEY_phone + " TEXT " + ")";



	final String CREATE_General_HOTEL_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_Hotel + "(" + KEY_id
			+ " INTEGER PRIMARY KEY , " + KEY_name + " TEXT , " + KEY_image_url
			+ " TEXT , " + KEY_address + " TEXT , " + KEY_phone + " TEXT , "
			+ KEY_lat + " TEXT , " + KEY_lon + " TEXT , " + KEY_island_id
			+ " TEXT , " + KEY_isFeature + " TEXT " + ")";

	final String CREATE_ISLAND_TABLE = "CREATE TABLE " + TABLE_Island + "("
			+ KEY_id + " INTEGER PRIMARY KEY , " + KEY_name + " TEXT , "
			+ KEY_image_url + " TEXT , " + KEY_logo + " TEXT , "
			+ KEY_description + " TEXT , " + KEY_discover_more + " TEXT , "
			+ KEY_discover_more_main_image + " TEXT , " + KEY_islandNo
			+ " INTEGER " + ")";

	final String CREATE_ISLAND_IMAGE_TABLE = "CREATE TABLE "
			+ TABLE_Island_Image + "(" + KEY_id + " INTEGER  , "
			+ KEY_image_url + " TEXT , " + KEY_serialid + " INTEGER , "
			+ KEY_type + " TEXT " + ")";

	final String CREATE_ISLAND_DETAILS_TABLE = "CREATE TABLE "
			+ TABLE_Island_Details + "(" + KEY_id + " INTEGER  , "
			+ KEY_islandDetaisId + " TEXT , " + KEY_title + " TEXT , "
			+ KEY_description + " TEXT , " + KEY_image_url + " TEXT , "
			+ KEY_serialid + " INTEGER " + ")";

	final String CREATE_General_Activity_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity + "(" + KEY_id + " INTEGER , "
			+ KEY_name + " TEXT , " + KEY_image_url + " TEXT , "
			+ KEY_image_title + " TEXT , " + KEY_description + " TEXT , "
			+ KEY_Activit_realId + " TEXT " + ")";

	final String CREATE_General_Activity_Feature_Island_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_Feature_island + "(" + KEY_id
			+ " INTEGER , " + KEY_island_id + " TEXT , " + KEY_island_name
			+ " TEXT , " + KEY_island_activity_id + " TEXT , "
			+ KEY_island_activity_title + " TEXT , "
			+ KEY_island_activity_image + " TEXT , "
			+ KEY_island_activity_description + " TEXT " + ")";

	final String CREATE_General_Activity_Thinks_To_Know_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_Things_to_Know + "(" + KEY_id
			+ " INTEGER  , " + KEY_thinks_to_know_Id + " TEXT , " + KEY_title
			+ " TEXT , " + KEY_description + " TEXT , " + KEY_image_url
			+ " TEXT " + ")";

	final String CREATE_General_Activity_types_of_activity_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_types_of_activity
			+ "("
			+ KEY_id
			+ " INTEGER  , "
			+ KEY_types_of_activity_Id
			+ " TEXT , "
			+ KEY_title
			+ " TEXT , "
			+ KEY_description
			+ " TEXT , "
			+ KEY_image_url + " TEXT " + ")";

	// Creating String for DIVING DIRECTORY SQL
	final String CREATE_DIVING_DIRECTORY_TABLE = "CREATE TABLE "
			+ TABLE_DIVING_DIR + " ( " + KEY_dd_id + " INTEGER , "
			+ KEY_dd_name + " TEXT , " + KEY_dd_description + " TEXT , "
			+ KEY_dd_address + " TEXT , " + KEY_dd_image_url + " TEXT , "
			+ KEY_dd_area + " TEXT , " + KEY_dd_city + " TEXT , " + KEY_dd_lat
			+ " TEXT , " + KEY_dd_lon + " TEXT , " + KEY_dd_email + " TEXT , "
			+ KEY_dd_fax + " TEXT , " + KEY_dd_phone + " TEXT , "
			+ KEY_dd_postal_code + " TEXT , " + KEY_dd_primary_contact
			+ " TEXT , " + KEY_dd_rates + " TEXT , " + KEY_dd_website
			+ " TEXT , " + KEY_dd_islandid + " TEXT ); ";
	//Transport table SQL create
		final String CREATE_TRANPORT_TABLE = "CREATE TABLE "
				+ TABLE_TRANSPORT + "(" + KEY_trans_keyId + " INTEGER PRIMARY KEY , "+ KEY_trans_spilt_type + " TEXT , "
				+ KEY_trans_island_id + " TEXT , " + KEY_trans_transport_type + " TEXT , "
				+ KEY_trans_transport_info + " TEXT " + ")";
	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_ISLAND_TABLE);
		db.execSQL(CREATE_ISLAND_IMAGE_TABLE);
		db.execSQL(CREATE_ISLAND_DETAILS_TABLE);
		db.execSQL(CREATE_General_Activity_TABLE);
		db.execSQL(CREATE_General_Activity_Feature_Island_TABLE);
		db.execSQL(CREATE_General_Activity_Thinks_To_Know_TABLE);
		db.execSQL(CREATE_General_Activity_types_of_activity_TABLE);
		db.execSQL(CREATE_General_HOTEL_TABLE);
		db.execSQL(CREATE_General_EVENT_TABLE);
		db.execSQL(CREATE_TRANPORT_TABLE);
		db.execSQL(CREATE_Category_TABLE);
		db.execSQL(CREATE_DIVING_DIRECTORY_TABLE);
		db.execSQL(CREATE_Category__Directory_TABLE);
		db.execSQL(CREATE_featureProperty_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Island);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Island_Image);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Island_Details);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_Feature_island);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_Things_to_Know);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_types_of_activity);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_Hotel);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Event);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIVING_DIR);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSPORT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Category);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Category_directory);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_featureProperty);
		// Create tables again
		onCreate(db);
	}
	
	public void addCategoryDirectory(String directoryId,String categoryId) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_directoryId, directoryId);
		values.put(KEY_categoryId, categoryId);
		// Inserting Row
		db.insert(TABLE_Category_directory, null, values);
		db.close(); // Closing database connection
	}
	
	public Vector<CategoryDirectoryInfo> getCategoryDirectoryList() {
		final Vector<CategoryDirectoryInfo> mIslandList = new Vector<CategoryDirectoryInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Category_directory;
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				CategoryDirectoryInfo pInfo = new CategoryDirectoryInfo();
				pInfo.setDirectoryId(cursor.getString(0));
				pInfo.setCategoryId(cursor.getString(1));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public void addCategory(String categoryId,String parentId,String title) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, categoryId);
		values.put(KEY_parentId, parentId);
		values.put(KEY_title, title);
		// Inserting Row
		db.insert(TABLE_Category, null, values);
		db.close(); // Closing database connection
	}

	
	public String getCategoryIdFromTitle(String title) {
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Category+" WHERE "+KEY_title+"='"+title+"'";
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);
		String id = "";
		if (cursor.moveToFirst()) {
			
			id=cursor.getString(0);
		}
		return id;
	}


	public boolean ifDivingDirExist(String dirId) {
		boolean result = false;

		openDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_DIVING_DIR + " WHERE " + KEY_dd_id
				+ " = '" + dirId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			result = true;

		} else {
			result = false;
		}

		closeDatabase();

		return result;

	}

	

	private void openDatabase() {
		// TODO Auto-generated method stub
		db = getWritableDatabase();

	}
	private void closeDatabase() {
		// TODO Auto-generated method stub
		if (db != null) {
			db.close();
		}// Closing database connection

	}

	public void addDivingDir(DivingDirectoryInfo dlist) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_dd_id, dlist.getId());
		values.put(KEY_dd_name, dlist.getName());
		values.put(KEY_dd_description, dlist.getDescription());
		values.put(KEY_dd_address, dlist.getAddress());
		values.put(KEY_dd_image_url, dlist.getImage_url());
		values.put(KEY_dd_area, dlist.getArea());
		values.put(KEY_dd_city, dlist.getCity());
		values.put(KEY_dd_lat, dlist.getLat());
		values.put(KEY_dd_lon, dlist.getLon());
		values.put(KEY_dd_email, dlist.getEmail());
		values.put(KEY_dd_fax, dlist.getFax());
		values.put(KEY_dd_phone, dlist.getPhone());
		values.put(KEY_dd_postal_code, dlist.getPostal_code());
		values.put(KEY_dd_primary_contact, dlist.getPrimary_contact());
		values.put(KEY_dd_rates, dlist.getRates());
		values.put(KEY_dd_website, dlist.getWebsite());
		values.put(KEY_dd_islandid, dlist.getIslandid());

		// Inserting Row
		db.insert(TABLE_DIVING_DIR, null, values);
		//printing message in log to ensure
		//print.message("Data Inserted");
		db.close(); // Closing database connection
	}
	
	public Vector<DivingDirectoryInfo> getDivingListFrmAcitivityId(String activityId) {

		Vector<DivingDirectoryInfo> mDivingDirDetailList = new Vector<>();
		mDivingDirDetailList.removeAllElements();
		final SQLiteDatabase db = getWritableDatabase();

		// Select All Query
		final String selectQuery = "SELECT  d.* FROM "+TABLE_DIVING_DIR+" as d,"+TABLE_Category_directory +" as cat_dir WHERE cat_dir."+KEY_categoryId+"='"+activityId+"' AND cat_dir."+KEY_directoryId+"=d."+KEY_dd_id;
//		final String selectQuery = "SELECT * FROM "+TABLE_DIVING_DIR+" INNER JOIN "
//				+TABLE_Category_directory +" ON "+TABLE_DIVING_DIR+"."+KEY_dd_id+"="+TABLE_Category_directory+"."+KEY_directoryId+" WHERE "+
//				TABLE_Category_directory+"."+KEY_categoryId+"='"+activityId+"'";
				
//		SELECT * FROM DivingDirectoryDB
//		INNER JOIN Category_directory ON 
//		DivingDirectoryDB.dd_id = Category_directory.directoryId
//		WHERE  Category_directory.categoryId = '1'
		Log.e("selectQuery", ">>"+selectQuery);

//		openDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				DivingDirectoryInfo pInfo = new DivingDirectoryInfo();

				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setAddress(cursor.getString(3));
				pInfo.setImage_url(cursor.getString(4));
				pInfo.setArea(cursor.getString(5));
				pInfo.setCity(cursor.getString(6));
				pInfo.setLat(cursor.getString(7));
				pInfo.setLon(cursor.getString(8));
				pInfo.setEmail(cursor.getString(9));
				pInfo.setFax(cursor.getString(10));
				pInfo.setPhone(cursor.getString(11));
				pInfo.setPostal_code(cursor.getString(12));
				pInfo.setPrimary_contact(cursor.getString(13));
				pInfo.setRates(cursor.getString(14));
				pInfo.setWebsite(cursor.getString(15));
				pInfo.setIslandid(cursor.getString(16));

				mDivingDirDetailList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}
		
		if (db != null) {
			db.close();
		}

		// return contact list
		return mDivingDirDetailList;
	}

	 public void deleteAllTransportInfo() {
	
		 openDatabase();
		 final String deleteAll = "DELETE " + "FROM "
		 + TABLE_TRANSPORT + " WHERE "
		 + KEY_trans_keyId + ">-1";
		
		 db.execSQL(deleteAll);
		 close();
	 }
	 
//	 public void deleteAllFeatureProperty() {
//			
//		 openDatabase();
//		 final String deleteAll = "DELETE " + "FROM " + TABLE_featureProperty + " WHERE " + KEY_id + ">-1";
//		
//		 db.execSQL(deleteAll);
//		 close();
//	 }
	


}
