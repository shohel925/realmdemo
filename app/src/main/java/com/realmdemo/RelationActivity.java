package com.realmdemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.realmdemo.adapter.DividerItemDecoration;
import com.realmdemo.db.DatabaseHandler;
import com.realmdemo.model.CategoryDirectoryInfo;
import com.realmdemo.model.DivingDirectoryInfo;
import com.realmdemo.model.DivingDirectoryResponse;
import com.realmdemo.model.UserDM;
import com.realmdemo.utils.AAPBDHttpClient;
import com.realmdemo.utils.AlertMessage;
import com.realmdemo.utils.AllURL;
import com.realmdemo.utils.BusyDialog;
import com.realmdemo.utils.NetInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import io.realm.Realm;
import io.realm.RealmResults;

public class RelationActivity extends AppCompatActivity {
    private Context context;
    private Realm realm;
    private BusyDialog busyNow;
    private DivingDirectoryResponse divingDirectoryResponse;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;
        db=new DatabaseHandler(context);
        realm=Realm.getDefaultInstance();
        initUI();
    }

    private void initUI() {
        getDivingDirectoryData(AllURL.getActivityDirectoryUrl());

    }

    protected void getDivingDirectoryData(final String url)
    {
        if (!NetInfo.isOnline(context)) {
            AlertMessage.showMessage(context, getString(R.string.app_name),getString(R.string.NoInternet));
            return;
        }

        Log.e("URL : ", url);
        busyNow = new BusyDialog(context, true,false);
        busyNow.show();

        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
            String response="";
            @Override
            public void run() {

             /* call API and Do background task.*/
                try {
                    response= AAPBDHttpClient.post(url).body();
                    Log.e("Response", ">>" + new String(response));
                } catch (Exception e) {
                    // TODO: handle exception
                }

                if (!TextUtils.isEmpty(new String(response))) {

                    try
                    {

                        Log.e("Response", ">>" + new String(response));
                        Gson g = new Gson();

                        divingDirectoryResponse = g.fromJson(new String(response),DivingDirectoryResponse.class);


                    }
                    catch (final Exception e)
                    {

                        e.printStackTrace();
                    }

	                /* Back to main thread/UI*/

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
	                        /* Update your UI*/
                            if (busyNow != null) {
                                busyNow.dismis();
                            }
                            if (divingDirectoryResponse.getStatus().equalsIgnoreCase("true")) {

                                for(DivingDirectoryInfo dlist:divingDirectoryResponse.getResult()){
                                    if(!db.ifDivingDirExist(dlist.getId())){
                                        db.addDivingDir(dlist);
                                        for(String activityId:dlist.getActivity_ids()){
                                            db.addCategoryDirectory(dlist.getId(), activityId);
                                        }
                                    }
                                }
								for(CategoryDirectoryInfo cInfo:db.getCategoryDirectoryList()){
									Log.e("CategoryId", ""+cInfo.getCategoryId());
									Log.e("directoryId", ""+cInfo.getDirectoryId());

								}

                            }
                            else {
                                AlertMessage.showMessage(context, getString(R.string.app_name), divingDirectoryResponse.getMessage() + "");
                            }


                        }
                    });
                }

            }
        });

    }

}
