package com.realmdemo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.realmdemo.adapter.DividerItemDecoration;
import com.realmdemo.model.UniversityDM;
import com.realmdemo.model.UserDM;
import com.realmdemo.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class UniversityActivity extends AppCompatActivity {
    private Context context;
    private EditText textviewUniversity;
    private RecyclerView recylerView_university;
    private UniversityAdapter userAdapter;
    private List<UniversityDM> tempList=new ArrayList<>();
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.university);
        context=this;
        realm=Realm.getDefaultInstance();
        initUI();
    }

    private void initUI() {
        textviewUniversity=(EditText)findViewById(R.id.textviewUniversity) ;
        recylerView_university=(RecyclerView)findViewById(R.id.recylerView_university);
        if (tempList!=null){
            tempList.clear();
        }
        // get all data from RealM ===============
        RealmResults<UniversityDM> data= realm.where(UniversityDM.class).findAll();
        tempList=realm.copyFromRealm(data);
        userAdapter=new UniversityAdapter(tempList);
        recylerView_university.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recylerView_university.setLayoutManager(mLayoutManager);
        recylerView_university.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recylerView_university.setItemAnimator(new DefaultItemAnimator());
        recylerView_university.setAdapter(userAdapter);
    }

    public void addButton(View view) {
        if (TextUtils.isEmpty(textviewUniversity.getText().toString())){
            Toast.makeText(context,"Please enter University Name",Toast.LENGTH_SHORT).show();
        }else{
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    UniversityDM dm=new UniversityDM();
                    dm.setUniversityName(textviewUniversity.getText().toString());
                    realm.copyToRealmOrUpdate(dm);
                    tempList.add(dm);

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    userAdapter.notifyDataSetChanged();
                    // Original queries and Realm objects are automatically updated.
                }
            });
        }
    }


    public class UniversityAdapter extends RecyclerView.Adapter<UniversityAdapter.MyViewHolder> {

        private List<UniversityDM> userList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView uName;

            public MyViewHolder(View view) {
                super(view);
                uName = (TextView) view.findViewById(R.id.uName);
            }
        }


        public UniversityAdapter(List<UniversityDM> moviesList) {
            this.userList = moviesList;
        }

        @Override
        public UniversityAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.university_row, parent, false);

            return new UniversityAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(UniversityAdapter.MyViewHolder holder, final int position) {
            final UniversityDM userDM = userList.get(position);
            holder.uName.setText(userDM.getUniversityName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("Log ","item CLick");
                    AppConstant.universityId=userList.get(position).getId();
                    startActivity(new Intent(context,StudentActivity.class));
                }
            });
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }
    }

}
